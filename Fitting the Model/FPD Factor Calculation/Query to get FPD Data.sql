select
fa.loan_num as loan_id
,fa.state_code
,fa.loan_actual_amount
,fa.effective_apr
,fa.loan_funded_date
,fa.first_payment_default
,fa.first_payment_due
,fa.previous_customer
,fa.previous_customer_group
,fa.source
,fau.dm_2_0_model_score_retroscored as dm2_0_score


,coalesce(rrop.risk_grade,
          case when fau.dm_2_0_model_score_retroscored is null then 'D'
               when fau.dm_2_0_model_score_retroscored < 36.9 then 'AAA'
               when fau.dm_2_0_model_score_retroscored < 42.13 then 'AA'
               when fau.dm_2_0_model_score_retroscored < 46.48 then 'A'
               when fau.dm_2_0_model_score_retroscored < 51.16 then 'B'
               when fau.dm_2_0_model_score_retroscored < 55.32 then 'C'
               else 'D' end) as risk_grade

,case when source.customer_type like '% Refinance' then 'Refinance'
           when source.customer_type = 'New - Lead' then 'Leads'
           when source.customer_type like 'New%' then 'New'
           else 'Previous' end as cust_type

from dw_reporting_views.fact_application fa
left join dw_reporting_views.fact_application_underwriting fau
      on fa.bsf_application_id = fau.bsf_application_id
left join dw_reporting_views.dim_application_source source
      on source.application_number = fa.application_number

left join dw_reporting_bsf_origination.application a
       on fa.bsf_application_id = a.application_id
       and now() <@ a.asserted
       and now() <@ a.effective
       and a.application_source_type_id = 2  --- will need to change this if I want anything non-leads
left join dw_reporting_bsf_leads.lead_rmodel_underwriting lru
       on lru.lead_id = a.application_source_id
left join dw_reporting_bsf_leads.rmodel_raw_output_parsed rrop
       on rrop.rmodel_raw_output_id = lru.rmodel_raw_output_id



where fa.loan_funded_date >= '2019-01-01'
and fa.first_payment_due = 1
and fa.lms = 'LP'
