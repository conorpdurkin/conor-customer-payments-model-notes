This repository contains a variety of analysis files for the customer payment model I built in March 2020 (which has since been updated a few times). 

Documentation around the files/contents can be found on a confluence page here: 

https://balancecredit.atlassian.net/wiki/spaces/AnalyticsTeam/pages/2425946125/Conor+s+Customer+Payments+Model