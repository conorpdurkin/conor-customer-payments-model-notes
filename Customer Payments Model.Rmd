

```{r setup, include=FALSE}
# Basic setup block.

rm(list = ls())

library(tidyr)
library(stringr)
library(plyr)
library(dplyr)
library(readr)
library(ggplot2)
library(DBI)
library(RPostgreSQL)

# set work directory
setwd("/Users/conordurkin/Desktop/Work/Customer Payments Model/*** Current Model")
time_start <- Sys.time()
```

```{r}

# Pulls data file directly from database, this sets up the connection. 
drv <- dbDriver('PostgreSQL')  
db <- 'enterprise_dw'  
host_db <- 'edw-adhoc.cjtyz5l7zjct.us-east-1.rds.amazonaws.com'  
db_port <- '5432'  
db_user <- 'conordurkin'  
db_password <- 'Cpd123sailboat'

conn <- dbConnect(drv, dbname=db, host=host_db, port=db_port, user=db_user, password=db_password)

query1 <- "
          select
          entity_id,
          period,
          period_start,
          period_end,
          payment_amount as payment,
          payment_i,
          payment_p,
          adb as balance_start,
          principal_balance as balance_end
          
          from dw_reporting_lp.loan_tx
          where type = 'forecastedPayment'
          and period_end >= current_date
          order by entity_id, period
"

query2 <- "
            with temp_as_of_date as (select current_date -1 as as_of_date)
            select
            stp.loan_id
            ,lsa.loan_status_text as loan_loanstatus
            ,lsa.loan_sub_status_text as loan_loansubstatus
            ,fa.state_code as state
            ,lsa.principal_balance as outstanding_principal
            ,lsa.periods_remaining as periods_remaining
            ,case when stp.payment_frequency = 'loan.frequency.monthly' then 'Monthly'
                  when stp.payment_frequency = 'loan.frequency.biWeekly' then 'Bi-Weekly'
                  when stp.payment_frequency = 'loan.frequency.semiMonthly' then 'Semi-Monthly' end as payment_frequency
            ,stp.contract_date as origination_date
            ,lsa.last_payment_date as last_succesful_pmt
            ,lsa.next_payment_date as next_scheduled_payment
            ,lsa.days_past_due as days_past_due
            ,case when lsa.days_past_due = 0 then 0
                  when lsa.days_past_due between 1 and 30 then 30
                  when lsa.days_past_due between 31 and 60 then 60
                  when lsa.days_past_due between 61 and 90 then 90
                  when lsa.days_past_due between 91 and 120 then 120
                  when lsa.days_past_due between 121 and 150 then 150
                  when lsa.days_past_due between 151 and 180 then 180
                  when lsa.days_past_due between 181 and 210 then 210
                  when lsa.days_past_due between 211 and 240 then 240
                  when lsa.days_past_due between 241 and 270 then 270
                  when lsa.days_past_due >= 271 then 300 end as aging_bucket
            ,case when source.customer_type like '% Refinance' then 'Refinance'
                       when source.customer_type = 'New - Lead' then 'Leads'
                       when source.customer_type like 'New%' then 'New'
                       else 'Previous' end as cust_type
            ,coalesce(fau.dm_2_0_model_score_prod, fau.dm_2_0_model_score_retroscored) as dm2_0_score --- note no leads scores pulled
            ,coalesce(rrop.risk_grade,
                      case when fau.dm_2_0_model_score_retroscored is null then 'D'
                           when fau.dm_2_0_model_score_retroscored < 36.9 then 'AAA'
                           when fau.dm_2_0_model_score_retroscored < 42.13 then 'AA'
                           when fau.dm_2_0_model_score_retroscored < 46.48 then 'A'
                           when fau.dm_2_0_model_score_retroscored < 51.16 then 'B'
                           when fau.dm_2_0_model_score_retroscored < 55.32 then 'C'
                           else 'D' end) as risk_grade
            ,fa.effective_apr
            ,fa.loan_number_of_payments
            ,fa.loan_actual_amount
            ,fa.loan_term_in_days
            ,fa.first_payment_date
            ,fa.payment_amount
            from dw_reporting_lp.loan_setup_entity stp
            left join dw_reporting_lp.loan_status_archive lsa
                   on stp.loan_id = lsa.loan_id
                  and lsa.date = (select as_of_date from temp_as_of_date)
            left join dw_reporting_views.fact_application fa
                   on fa.lms = 'LP'
                  and fa.loan_num = lsa.loan_id
            left join dw_reporting_views.dim_application_source source
                   on source.application_number = fa.application_number
            left join dw_reporting_views.fact_application_underwriting fau
                   on fa.bsf_application_id = fau.bsf_application_id
            left join dw_reporting_bsf_origination.application a
                   on fa.bsf_application_id = a.application_id
                   and now() <@ a.asserted
                   and now() <@ a.effective
                   and a.application_source_type_id = 2  --- will need to change this if I want anything non-leads
            left join dw_reporting_bsf_leads.lead_rmodel_underwriting lru
                   on lru.lead_id = a.application_source_id
            left join dw_reporting_bsf_leads.rmodel_raw_output_parsed rrop
                   on rrop.rmodel_raw_output_id = lru.rmodel_raw_output_id
            where stp.contract_date <= (select as_of_date from temp_as_of_date)
              and lsa.loan_status_text = 'Active'
              and fa.state_code != 'KS'  --- unfortunately my stuff doesn't really work for kansas (LOC).
"

schedules <- dbGetQuery(conn, query1)
portfolio <- dbGetQuery(conn, query2)
dbDisconnect(conn)

# Archive the inputs
# write_csv(schedules, paste0('Inputs for R/archive/future_loan_schedules.',Sys.Date(),'.csv'))
# write_csv(portfolio, paste0('Inputs for R/archive/portfolio.',Sys.Date(),'.csv'))

```

```{r}
# Building model inputs.
# Set an FPD baseline, read in FPD factors and performance curve data. 
fpd_baseline <- .1507

fpd_factor_customer <- read_csv('Inputs for R/fpd_factors/fpd_customer_factors.csv')
fpd_factor_grade <- read_csv('Inputs for R/fpd_factors/fpd_grade_factors.csv')
fpd_factor_state <- read_csv('Inputs for R/fpd_factors/fpd_state_factors.csv')

curedata <- read_csv('Inputs for R/Curves/cure_rates.csv')
marg_def_index <- read_csv('Inputs for R/Curves/marginal_default_index_curve.csv')
prepay_rate <- read_csv('Inputs for R/Curves/prepay_rate_curve.csv')
refi_rate <- read_csv('Inputs for R/Curves/refi_rate_curve.csv')

```

```{r}
# Add FPD factors to the Loan Portfolio 
portfolio <- merge(portfolio, fpd_factor_customer, by.x = 'cust_type', by.y = 'Customer') %>%
             merge(fpd_factor_grade, by.x = 'risk_grade', by.y = 'Grade') %>%
             merge(fpd_factor_state, by.x = 'state', by.y = 'State') 

# Estimate FPDs from FPD factors, clean up the data so things work. Set cure rates (allowing for a 3 day lag in payment posting)
portfolio <- portfolio %>% mutate(est_fpd = portfolio$CustFactor * portfolio$StateFactor * portfolio$GradeFactor * fpd_baseline,
                                  periods_completed = loan_number_of_payments - periods_remaining,
                                  periods_completed_censored = ifelse(periods_completed > 30, 30, periods_completed),
                                  periods_censored_cures = ifelse(periods_completed > 6, 6, periods_completed)) %>% 
                           merge(curedata, 
                                 by.x = c("periods_censored_cures", "risk_grade"), 
                                 by.y = c("installment_number", "dm2_0_riskgrade"), 
                                 all.x = TRUE) %>%
                           mutate(cure_rate = ifelse(days_past_due < 3, 1, NA),
                                  cure_rate = ifelse(days_past_due < 10 & is.na(cure_rate), cure_d0, cure_rate),
                                  cure_rate = ifelse(days_past_due < 18 & is.na(cure_rate), cure_d7, cure_rate),
                                  cure_rate = ifelse(days_past_due < 33 & is.na(cure_rate), cure_d15, cure_rate),
                                  cure_rate = ifelse(days_past_due < 63 & is.na(cure_rate), cure_d30, cure_rate),
                                  cure_rate = ifelse(days_past_due >= 63, 0, cure_rate),
                                  cure_rate = ifelse(cure_rate < 0, 0, cure_rate))
                                  
# Combine the portfolio with the future payment schedules.
portfolio_forecast <- merge(portfolio, schedules, by.x = 'loan_id', by.y = 'entity_id') %>%
                      mutate(period_censored = ifelse(period > 30, 30, period),
                             payment_date = period_end + 1)

# If needed, this specific line of code is what applies any default scenario stresses to the portfolio. Can be adjusted as needed.
portfolio_forecast <- portfolio_forecast %>% mutate(default_stress = 1) 

# Merge in the marginal default index, refi rate, and prepay rates.
portfolio_forecast <- portfolio_forecast %>% merge(marg_def_index, by.x = c("period_censored", "risk_grade"), by.y = c("installment_number", "Grade")) %>%
                                             merge(prepay_rate, by.x = c("period_censored", "risk_grade"), by.y = c("installment_number", "Grade")) %>%
                                             merge(refi_rate, by.x = c("period_censored", "risk_grade"), by.y = c("installment_number", "Grade")) %>% 
                                             mutate(marginal_default_rate = (marg_def_index * est_fpd * default_stress))


# Next generate the 'survival rates' (% of portfolio still outstanding at start/end of each period) for every loan. 
portfolio_forecast <- portfolio_forecast %>% arrange(loan_id, period)
survival_start <- numeric(nrow(portfolio_forecast))
survival_end <- numeric(nrow(portfolio_forecast))

survival_start[1] <- 1
survival_end[1] <- 1 - portfolio_forecast$marginal_default_rate[1] -
                       portfolio_forecast$prepayrate[1] - 
                       portfolio_forecast$refirate[1]


for (i in 2:nrow(portfolio_forecast)){
  if (portfolio_forecast$loan_id[i] != portfolio_forecast$loan_id[(i - 1)]){
      survival_start[i] = 1
      survival_end[i] = (1 - portfolio_forecast$marginal_default_rate[i] -
                             portfolio_forecast$prepayrate[i] -
                             portfolio_forecast$refirate[i])
  } else{
      survival_start[i] = survival_end[i - 1]
      survival_end[i] = survival_start[i] * (1 - portfolio_forecast$marginal_default_rate[i] -
                                                 portfolio_forecast$prepayrate[i] -
                                                portfolio_forecast$refirate[i])
  }}

portfolio_forecast <- portfolio_forecast %>%
                      cbind(survival_start, survival_end) %>%
                      mutate(survival_start = ifelse(survival_start < 0, 0, survival_start),
                             survival_end = ifelse(survival_end < 0, 0, survival_end))


# Generate the expected collected payment for each loan/payment date, based on "scheduled payment" times survival rate (and some other stuff)
portfolio_forecast <- portfolio_forecast %>% mutate(expected_collections = cure_rate * ((payment * survival_start * (1 - marginal_default_rate)) + 
                                                                                        (balance_start * survival_end * (prepayrate + refirate))))

# Aggregate cash collections up on a day by day basis at a portfolio level
daily_cash_forecast <- portfolio_forecast %>% mutate(date = payment_date) %>%
                                              group_by(date) %>%
                                              summarise(cash = sum(expected_collections))

#Save CSV containing the daily cash forecast
 write.csv(daily_cash_forecast, paste0("portfolio_forecast_", Sys.Date(),".csv"))

time_end <- Sys.time()
```
